package com.example.hw2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class B extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        Intent intent1 = getIntent();
        String text = intent1.getStringExtra("MAIN_EXTRA");

        TextView tv = findViewById(R.id.textViewB);
        tv.setText(text);

        Button btnPrev = findViewById(R.id.buttonPreviousB);
        Button btnNext = findViewById(R.id.buttonNextB);

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(B.this, A.class);
                intent.putExtra("MAIN_EXTRA", "From B Activity");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(B.this, C.class);
                intent.putExtra("MAIN_EXTRA", "From B Activity");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onPause(){
        super.onPause();
        Log.d("Log Pause B", "See you soon on Activity B");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("Log Resume B", "See you soon on Activity B");
    }

}