package com.example.hw2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class C extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c);

        Intent intent1 = getIntent();
        String text = intent1.getStringExtra("MAIN_EXTRA");

        TextView tv = findViewById(R.id.textViewC);
        tv.setText(text);

        Button btnPrev = findViewById(R.id.buttonPreviousC);
        Button btnNext = findViewById(R.id.buttonNextC);

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(C.this, B.class);
                intent.putExtra("MAIN_EXTRA", "From C Activity");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(C.this, MainActivity.class);
                intent.putExtra("MAIN_EXTRA", "From C Activity");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onPause(){
        super.onPause();
        Log.d("Log Pause C", "See you soon on Activity C");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("Log Resume C", "See you soon on Activity C");
    }

}